<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Answer;
use AppBundle\Entity\Question;
use AppBundle\Entity\Survey;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $surveys = $this->getDoctrine()
            ->getRepository('AppBundle:Survey')
            ->findAll();

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'surveys' => $surveys
        ]);
    }

    /**
     * @Route("/umfrage/{survey}", name="show")
     * @ParamConverter()
     */
    public function showOrSaveAction(Request $request, Survey $survey)
    {
        $this->getDoctrine()->getManager()->persist($survey);
        $form = $this->createForm('AppBundle\Form\SurveyType', $survey);

        $form->handleRequest($request);
        if ($form->isValid()) {
            foreach($survey->getQuestions() as $question) {
                /** @var Question $question */
                foreach($question->getAnswers() as $answer) {
                    /** @var Answer $answer */
                    if ($answer->isSelected()) {
                        $answer->vote();
                    }
                    $this->getDoctrine()->getManager()->persist($answer);
                }
            }
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('results', array('survey' => $survey->getId()));
        }

        return $this->render('default/show.html.twig', [
            'survey' => $survey,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/results/{survey}", name="results")
     * @ParamConverter()
     */
    public function resultAction(Request $request, Survey $survey)
    {
        return $this->render('default/results.html.twig', [
            'questions' => $survey->getQuestions()
        ]);
    }
    
}
