<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Answer;
use AppBundle\Entity\Question;
use AppBundle\Entity\Survey;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="admin")
     */
    public function indexAction()
    {
        // load surveys:
        $surveys = $this->getDoctrine()
          ->getRepository('AppBundle:Survey')
          ->findAll();

        return $this->render(':admin:index.html.twig', array('surveys' => $surveys));
    }

    /**
     *
     * @param Request $request
     * @param Survey $survey
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/survey/{survey}", name="create_survey")
     */
    public function newOrEditAction(Request $request, Survey $survey=null)
    {
        if($survey == null) {
            $survey = new Survey();
        }
        else {
            $survey = $this->getDoctrine()
              ->getRepository('AppBundle:Survey')
              ->find($survey);
        }

        $form = $this->createFormBuilder($survey)
          ->add('author', TextType::class)
          ->add('title', TextType::class)
          ->add('save', SubmitType::class, array('label' => 'Create Survey'))
          ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->persist($survey);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('admin');
        }
        return $this->render(':admin:create_edit_form.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param Request $request
     * @param Survey $survey
     * @Route("/survey/delete/{survey}", name="delete_survey")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Survey $survey)
    {
        // delete survey:
        $survey = $this->getDoctrine()
          ->getRepository('AppBundle:Survey')
          ->find($survey);
        $this->getDoctrine()->getManager()->remove($survey);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('admin');
    }


    /**
     *
     * @param Request $request
     * @param Question $question
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/question/{question}", name="create_question")
     */
    public function newOrEditQuestionAction(Request $request, Question $question=null)
    {
        if($question == null) {
            $question = new Question();
        }
        else {
            $question = $this->getDoctrine()
              ->getRepository('AppBundle:Question')
              ->find($question);
        }

        $form = $this->createFormBuilder($question)
          ->add('text', TextType::class)
          ->add('survey', EntityType::class, array(
            'class' => 'AppBundle:Survey',
            'choice_label' => 'title',
          ))
          ->add('multiple_choice', CheckboxType::class, array('required' => false))
          ->add('save', SubmitType::class, array('label' => 'Create Question'))
          ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->persist($question);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('admin');
        }
        return $this->render(':admin:create_edit_form.html.twig', array('form' => $form->createView()));
    }


    /**
     * @param Request $request
     * @param Question $question
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/question/delete/{question}", name="delete_question")
     */
    public function deleteQuestionAction(Request $request, Question $question)
    {
        // delete survey:
        $question = $this->getDoctrine()
          ->getRepository('AppBundle:Question')
          ->find($question);
        $this->getDoctrine()->getManager()->remove($question);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('admin');
    }

    /**
     *
     * @param Request $request
     * @param Answer $answer
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/answer/{answer}", name="create_answer")
     */
    public function newOrEditAnswerAction(Request $request, Answer $answer=null)
    {
        if($answer == null) {
            $answer = new Answer();
        }
        else {
            $answer = $this->getDoctrine()
              ->getRepository('AppBundle:Answer')
              ->find($answer);
        }

        $form = $this->createFormBuilder($answer)
          ->add('text', TextType::class)
          ->add('question', EntityType::class, array(
            'class' => 'AppBundle:Question',
            'choice_label' => 'text',
          ))
          ->add('save', SubmitType::class, array('label' => 'Create Answer'))
          ->getForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->persist($answer);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('admin');
        }
        return $this->render(':admin:create_edit_form.html.twig', array('form' => $form->createView()));
    }

    /**
     * @param Request $request
     * @param Answer $answer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/answer/delete/{answer}", name="delete_answer")
     */
    public function deleteAnswerAction(Request $request, Answer $answer)
    {
        // delete survey:
        $answer = $this->getDoctrine()
          ->getRepository('AppBundle:Answer')
          ->find($answer);
        $this->getDoctrine()->getManager()->remove($answer);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('admin');
    }
}
