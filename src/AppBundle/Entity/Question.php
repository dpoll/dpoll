<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestionRepository")
 *
 */
class Question
{
    /**
     * @ORM\OneToMany(targetEntity="Answer", mappedBy="question")
     */
    protected $answers;

    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }

    /**
    * @ORM\ManyToOne(targetEntity="Survey", inversedBy="questions")
    * @ORM\JoinColumn(name="survey_id", referencedColumnName="id", onDelete="CASCADE")
    */
    protected $survey;

    /**
     * @var boolean
     *
     * @ORM\Column(name="multiple_choice", type="boolean", options={"default":false})
     */
    private $multipleChoice;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255)
     */
    private $text;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Question
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set survey
     *
     * @param \AppBundle\Entity\Survey $survey
     *
     * @return Question
     */
    public function setSurvey(Survey $survey = null)
    {
        $this->survey = $survey;

        return $this;
    }

    /**
     * Get survey
     *
     * @return \AppBundle\Entity\Survey
     */
    public function getSurvey()
    {
        return $this->survey;
    }

    /**
     * Add answer
     *
     * @param \AppBundle\Entity\Answer $answer
     *
     * @return Question
     */
    public function addAnswer(Answer $answer)
    {
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \AppBundle\Entity\Answer $answer
     */
    public function removeAnswer(Answer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Set multipleChoice
     *
     * @param boolean $multipleChoice
     *
     * @return Question
     */
    public function setMultipleChoice($multipleChoice)
    {
        $this->multipleChoice = $multipleChoice;

        return $this;
    }

    /**
     * Get multipleChoice
     *
     * @return boolean
     */
    public function getMultipleChoice()
    {
        return $this->multipleChoice;
    }

    public function totalVoteCount()
    {
        $totalVoteCount = 0;
        foreach($this->getAnswers() as $answer) {
            $totalVoteCount += $answer->getVoteCount();
        }
        return $totalVoteCount;
    }

    /**
     * @Assert\Callback
     * @param ExecutionContextInterface $context
     */
    public function areChoicesValid(ExecutionContextInterface $context)
    {
        $checkcount = 0;
        foreach ($this->getAnswers() as $answer) {
            if ($answer->isSelected())
            {
                $checkcount++;
            }
        }

        if ($checkcount>1 && !$this->getMultipleChoice()) {
            $context->buildViolation('Es wurde mehr als eine Antwortmöglichkeit ausgewählt.')
              ->atPath('answers')
              ->addViolation();
        }
    }
}
