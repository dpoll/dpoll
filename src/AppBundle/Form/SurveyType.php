<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SurveyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('questions', 'Symfony\Component\Form\Extension\Core\Type\CollectionType', array(
            'entry_type' => 'AppBundle\Form\QuestionType',

        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Survey',
            'cascade_validation' => true
        ));
    }

    public function getName()
    {
        return 'survey';
    }
}