<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('answers', 'Symfony\Component\Form\Extension\Core\Type\CollectionType', array(
            'label' => false,
            'entry_type' => 'AppBundle\Form\AnswerType',
            'error_bubbling' => false
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Question',
        ));
    }

    public function getName()
    {
        return 'question';
    }
}
