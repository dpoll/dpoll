var gulp = require('gulp');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var less = require('gulp-less');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var del = require('del');
var merge = require('merge-stream');

//JAVASCRIPT TASK: write one minified js file out of jquery.js, bootstrap.js and all of my custom js files
gulp.task('js', function () {
    return gulp.src(['bower_components/jquery/dist/jquery.js',
            'bower_components/bootstrap/dist/js/bootstrap.js',
            'app/Resources/public/js/**/*.js'])
        .pipe(concat('javascript.js'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('web/js'));
});

gulp.task('build-css', function () {

    var lessStream = gulp.src([
            'bower_components/bootstrap/dist/css/bootstrap.css',
            'app/Resources/public/less/**/*.less'])
        .pipe(gulpif(/[.]less/, less()))
        .pipe(concat('less.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('web/css'));


    var scssStream = gulp.src('app/Resources/public/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('sass.css'))
        .pipe(gulp.dest('web/css'));


    return merge(lessStream, scssStream)
        .pipe(concat('styles.css'))
        .pipe(uglifycss())
        .pipe(gulp.dest('web/css'));

});

//IMAGE TASK: Just pipe images from project folder to public web folder
gulp.task('img', function() {
    return gulp.src('app/Resources/public/img/**/*.*')
        .pipe(gulp.dest('web/img'));
});

gulp.task('css',['build-css'], function() {
    return del([
        'web/css/sass.css',
        'web/css/less.css'
    ]);
});

gulp.task('fonts', function() {
    return gulp.src('bower_components/bootstrap/dist/fonts/*')
        .pipe(gulp.dest('web/fonts'));
});
//define executable tasks when running "gulp" command
//gulp.task('default', ['js', 'css', 'img', 'sass']);
gulp.task('default', ['js', 'img', 'css', 'fonts']);